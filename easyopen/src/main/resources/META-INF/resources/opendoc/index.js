var title = document.title;
// 左边树
$(document).ready(function() {
    $('#tree').ztree_toc({
        _header_nodes: [{
            id: 1,
            pId: 0,
            name: title,
            open: false
        }],
        // 第一个节点
        ztreeSetting: {
            view: {
                dblClickExpand: false,
                showLine: true,
                showIcon: false,
                selectedMulti: false
            },
            data: {
                simpleData: {
                    enable: true,
                    idKey: "id",
                    pIdKey: "pId"
                    // rootPId: "0"
                }
            },
            callback: {
                beforeClick: function(treeId, treeNode) {
                    $('a').removeClass('curSelectedNode');
                    if (treeNode.id == 1) {
                        $('body').scrollTop(0);
                    }
                    if ($.fn.ztree_toc.defaults.is_highlight_selected_line == true) {
                        $('#' + treeNode.id).css('color', 'red').fadeOut("slow",
                        function() {
                            $(this).show().css('color', 'black');
                        });
                    }
                }
            }
        },
        is_auto_number: false,
        // 菜单是否显示编号，如果markdown标题没有数字标号可以设为true
        documment_selector: '.markdown-body',
        is_expand_all: true // 菜单全部展开
    });
    
    // 代码高亮
    $('.highlight').each(function(i, block) {
        hljs.highlightBlock(block);
    });
    
    // 请求按钮
    $('.api-block').find('.post-btn').click(function() {
        var _parent = $(this).parents('.api-block');
        var name = _parent.find('.api-name').val();
        var version = _parent.find('.api-version').val();

        var jsonData = {};
        var formArray = _parent.find('form').serializeArray();

        $.each(formArray,
        function(i, field) {
            jsonData[field.name] = field.value;
        });

        var _param = _parent.find('.api-data');
        var _result = _parent.find('.api-result');
        var _postResult = _parent.find('.post-result');

        sdk.config({
            url: $('#url').val(),
            app_key: $('#appKey').val(),
            secret: $('#secret').val(),
        });

        sdk.post({
            name: name // 接口名
            ,version:version
            ,access_token:$('#access_token').val()
            ,data: jsonData // 请求参数
            ,callback: function(resp, postDataStr) { // 成功回调
            	_param.val(postDataStr);
                _result.val(formatJson(JSON.stringify(resp)));
                _postResult.show();
            }
        });
        return false;
    });
    
    $('.api-item').find('.new-win-btn').click(function(){
    	var $div = $(this).parents('.api-item');
    	
    	window.open(ctx + "/opendoc/new.html?id=" + $div.attr('id'))
    	
    	return false;
    });
});