package com.gitee.easyopen;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 处理请求
 * @author tanghc
 *
 */
public interface Invoker extends HasConfig {
    
    /**
     * 调用接口方法
     * 
     * @param request
     * @param response
     * @return 返回最终业务结果
     */
    Object invoke(HttpServletRequest request, HttpServletResponse response);
   
    /**
     * 将异常转换成结果
     * @param e
     * @return 返回对象
     */
    Result caugthException(Throwable e);
}
