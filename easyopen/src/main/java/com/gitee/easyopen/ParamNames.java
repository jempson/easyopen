package com.gitee.easyopen;

/**
 * 请求参数名定义
 * @author tanghc
 *
 */
public class ParamNames {
    public static String API_NAME = "name";

    public static String VERSION_NAME = "version";

    public static String APP_KEY_NAME = "app_key";

    public static String DATA_NAME = "data";

    public static String TIMESTAMP_NAME = "timestamp";
    
    public static String SIGN_NAME = "sign";
    
    public static String FORMAT_NAME = "format";
    
    public static String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss";
    
    public static String ACCESS_TOKEN_NAME = "access_token";
    
    public static String SIGN_METHOD_NAME = "sign_method";
    
}
