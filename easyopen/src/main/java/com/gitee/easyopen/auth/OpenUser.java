package com.gitee.easyopen.auth;

import java.io.Serializable;

/**
 * 如果要使用oauth2功能，自己的用户类需要实现这个对象
 * @author tanghc
 *
 */
public interface OpenUser extends Serializable {
    String getUsername();
}
