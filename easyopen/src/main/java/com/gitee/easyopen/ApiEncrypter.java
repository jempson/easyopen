package com.gitee.easyopen;

import java.security.interfaces.RSAPrivateKey;

import com.gitee.easyopen.util.AESUtil;
import com.gitee.easyopen.util.MD5Util;
import com.gitee.easyopen.util.RSAUtil;

/**
 * 负责各类加解密
 * @author tanghc
 *
 */
public class ApiEncrypter implements Encrypter {

    @Override
    public RSAPrivateKey getPrivateKey(String priKey) throws Exception {
        return RSAUtil.getPrivateKey(priKey);
    }

    @Override
    public String aesEncryptToHex(String content, String password) throws Exception {
        return AESUtil.encryptToHex(content, password);
    }

    @Override
    public String aesDecryptFromHex(String hex, String password) throws Exception {
        return AESUtil.decryptFromHex(hex, password);
    }

    @Override
    public String rsaDecryptByPrivateKey(String data, RSAPrivateKey privateKey) throws Exception {
        return RSAUtil.decryptByPrivateKey(data, privateKey);
    }

    @Override
    public String rsaEncryptByPrivateKey(String data, RSAPrivateKey privateKey) throws Exception {
        return RSAUtil.encryptByPrivateKey(data, privateKey);
    }

    @Override
    public String md5(String value) {
        return MD5Util.encrypt(value);
    }

}
