package com.gitee.easyopen.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.log.NullLogChute;

/**
 * Velocity工具类,根据模板内容生成文件
 */
public class VelocityUtil {

    static {
        // 禁止输出日志
        Velocity.setProperty(Velocity.RUNTIME_LOG_LOGSYSTEM, new NullLogChute());
        Velocity.init();
    }

    private static String LOG_TAG = VelocityUtil.class.getName();

    public static void generate(VelocityContext context, InputStream inputStream, Writer writer) {
        Reader reader = new InputStreamReader(inputStream);

        Velocity.evaluate(context, writer, LOG_TAG, reader);

        try {
            writer.close();
            reader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
