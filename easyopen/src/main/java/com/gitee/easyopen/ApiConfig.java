package com.gitee.easyopen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import com.gitee.easyopen.auth.Oauth2Manager;
import com.gitee.easyopen.auth.Oauth2Service;
import com.gitee.easyopen.auth.impl.Oauth2ServiceImpl;
import com.gitee.easyopen.interceptor.ApiInterceptor;
import com.gitee.easyopen.jwt.JwtService;
import com.gitee.easyopen.jwt.impl.JwtServiceImpl;
import com.gitee.easyopen.monitor.ApiMonitorStore;
import com.gitee.easyopen.monitor.MonitorInterceptor;
import com.gitee.easyopen.monitor.MonitorStore;
import com.gitee.easyopen.serializer.JsonResultSerializer;
import com.gitee.easyopen.serializer.XmlResultSerializer;
import com.gitee.easyopen.session.ApiSessionManager;
import com.gitee.easyopen.session.SessionManager;

/**
 * 配置类,所有配置相关都在这里.
 * 
 * @author tanghc
 *
 */
public class ApiConfig {
    /**
     * 加密工具
     */
    private Encrypter encrypter = new ApiEncrypter();
    /**
     * app秘钥管理
     */
    private AppSecretManager appSecretManager = new CacheAppSecretManager();
    /**
     * 返回结果
     */
    private ResultCreator resultCreator = new ApiResultCreator();
    /**
     * json序列化
     */
    private ResultSerializer jsonResultSerializer = new JsonResultSerializer();
    /**
     * xml序列化
     */
    private ResultSerializer xmlResultSerializer = new XmlResultSerializer();
    /**
     * 签名工具
     */
    private Signer signer = new ApiSigner();

    private RespWriter respWriter = new ApiRespWriter();
    /**
     * 参数解析
     */
    private ParamParser paramParser = new ApiParamParser();

    /**
     * session管理
     */
    private SessionManager sessionManager = new ApiSessionManager();
    
    /**
     * 拦截器
     */
    private ApiInterceptor[] interceptors = {};
    

    /**
     * oauth2服务端默认实现
     */
    private Oauth2Service oauth2Service = new Oauth2ServiceImpl();

    
    /**
     * 存储监控信息
     */
    private MonitorStore monitorStore = new ApiMonitorStore();
    
    /**
     * 错误模块
     */
    private List<String> isvModules = new ArrayList<String>();
    
    /* ************在构造方法中初始化************ */
    /**
     * 负责监控的拦截器
     */
    private ApiInterceptor monitorInerceptor;
    
    /**
     * 校验接口
     */
    private Validator validator;
    
    /**
     * JWT处理 
     */
    private JwtService jwtService;
    /**
     * 请求转发
     */
    private Invoker invoker;
    /**
     * oauth2认证服务，需要自己实现
     */
    private Oauth2Manager oauth2Manager;
    /* ***************************** */
    
    
    {
        isvModules.add("i18n/isv/error");
    };

    /**
     * 默认版本号
     */
    private String defaultVersion = "";
    
    /**
     * 超时时间
     */
    private int timeoutSeconds = 3;

    /**
     * 是否生成doc文档
     */
    private boolean showDoc;
    
    /**
     * 文档模板路径
     */
    private String docClassPath = "/easyopen_template/index.html";
    
    /**
     * 监控模板路径
     */
    private String monitorClassPath = "/easyopen_template/monitor.html";

    /**
     * 忽略验证
     */
    private boolean ignoreValidate;

    /**
     * 登录视图页面用于，mvc视图，如：loginView
     */
    private String oauth2LoginUri = "/oauth2login";

    /**
     * oauth2的accessToken过期时间,单位秒,默认2小时
     */
    private long oauth2ExpireIn = 7200;

    /**
     * jwt过期时间,秒,默认2小时
     */
    private int jwtExpireIn = 7200;
    /**
     * RSA加密对应的私钥
     */
    private String privateKey;

    /**
     * 私钥文件存放的classpath地址
     */
    private String priKeyPath = "/pri.key";
    
    /**
     * 是否开启监控
     */
    private boolean showMonitor = true;
    /**
     * 进入监控页面密码
     */
    private String monitorPassword = "monitor123";
    /**
     * 存放监控错误信息队列长度。超出长度，新值替换旧值
     */
    private int monitorErrorQueueSize = 5;
    /**
     * 处理线程池大小
     */
    private int monitorExecutorSize = 2;
    
    public ApiConfig() {
        monitorInerceptor = new MonitorInterceptor(this);
        validator = new ApiValidator(this);
        jwtService = new JwtServiceImpl(this);
        invoker = new ApiInvoker(this);
    }

    /**
     * 开启app对接模式，开启后不进行timeout校验。<br>
     * 如果平台直接跟Android或IOS对接，可开启这个功能。因为手机上的时间有可能跟服务端的时间不一致（用户的手机情况不可控）。<br>
     * 失去了时间校验，一个请求有可能被反复调用，服务端需要防止重复提交，有必要的话上HTTPS。
     */
    public void openAppMode() {
        this.timeoutSeconds = 0;
    }
    
    public Encrypter getEncrypter() {
        return encrypter;
    }

    public void setEncrypter(Encrypter encrypter) {
        this.encrypter = encrypter;
    }
    
    public void loadPrivateKey() {
        ClassPathResource res = new ClassPathResource(this.priKeyPath);
        if(res.exists()) {
            try {
                this.privateKey = IOUtils.toString(res.getInputStream(), "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void initOauth2Service(Oauth2Manager oauth2Manager) {
        this.oauth2Manager = oauth2Manager;
        this.oauth2Service.setOauth2Manager(oauth2Manager);
    }
    
    public boolean isShowDoc() {
        return showDoc;
    }

    public void setShowDoc(boolean showDoc) {
        this.showDoc = showDoc;
    }
    
    public boolean isShowMonitor() {
        return showMonitor;
    }

    public void setShowMonitor(boolean showMonitor) {
        this.showMonitor = showMonitor;
    }

    /**
     * 添加秘钥配置，map中存放秘钥信息，key对应appKey，value对应secret
     * 
     * @param appSecretStore
     */
    public void addAppSecret(Map<String, String> appSecretStore) {
        this.appSecretManager.addAppSecret(appSecretStore);
    }

    public AppSecretManager getAppSecretManager() {
        return appSecretManager;
    }

    public void setAppSecretManager(AppSecretManager appSecretManager) {
        this.appSecretManager = appSecretManager;
    }

    public ResultCreator getResultCreator() {
        return resultCreator;
    }

    public void setResultCreator(ResultCreator resultCreator) {
        this.resultCreator = resultCreator;
    }

    public int getTimeoutSeconds() {
        return timeoutSeconds;
    }

    public void setTimeoutSeconds(int timeoutSeconds) {
        this.timeoutSeconds = timeoutSeconds;
    }

    public ResultSerializer getJsonResultSerializer() {
        return jsonResultSerializer;
    }

    public void setJsonResultSerializer(ResultSerializer jsonResultSerializer) {
        this.jsonResultSerializer = jsonResultSerializer;
    }

    public ResultSerializer getXmlResultSerializer() {
        return xmlResultSerializer;
    }

    public void setXmlResultSerializer(ResultSerializer xmlResultSerializer) {
        this.xmlResultSerializer = xmlResultSerializer;
    }

    public List<String> getIsvModules() {
        return isvModules;
    }

    public void setIsvModules(List<String> isvModules) {
        this.isvModules = isvModules;
    }

    public boolean isIgnoreValidate() {
        return ignoreValidate;
    }

    public void setIgnoreValidate(boolean ignoreValidate) {
        this.ignoreValidate = ignoreValidate;
    }

    public RespWriter getRespWriter() {
        return respWriter;
    }

    public void setRespWriter(RespWriter respWriter) {
        this.respWriter = respWriter;
    }

    public String getOauth2LoginUri() {
        return oauth2LoginUri;
    }

    public void setOauth2LoginUri(String oauth2LoginUri) {
        this.oauth2LoginUri = oauth2LoginUri;
    }

    public long getOauth2ExpireIn() {
        return oauth2ExpireIn;
    }

    public void setOauth2ExpireIn(long oauth2ExpireIn) {
        this.oauth2ExpireIn = oauth2ExpireIn;
    }

    public Oauth2Service getOauth2Service() {
        return oauth2Service;
    }

    public void setOauth2Service(Oauth2Service oauth2Service) {
        this.oauth2Service = oauth2Service;
    }

    public Oauth2Manager getOauth2Manager() {
        return oauth2Manager;
    }

    public void setOauth2Manager(Oauth2Manager oauth2Manager) {
        this.oauth2Manager = oauth2Manager;
    }

    public int getJwtExpireIn() {
        return jwtExpireIn;
    }

    public void setJwtExpireIn(int jwtExpireIn) {
        this.jwtExpireIn = jwtExpireIn;
    }

    public JwtService getJwtService() {
        return jwtService;
    }

    public void setJwtService(JwtService jwtService) {
        jwtService.setApiConfig(this);
        this.jwtService = jwtService;
    }
    

    public Validator getValidator() {
        return validator;
    }

    public void setValidator(Validator validator) {
        validator.setApiConfig(this);
        this.validator = validator;
    }
    
    public Invoker getInvoker() {
        return invoker;
    }

    public void setInvoker(Invoker invoker) {
        invoker.setApiConfig(this);
        this.invoker = invoker;
    }

    public Signer getSigner() {
        return signer;
    }

    public void setSigner(Signer signer) {
        this.signer = signer;
    }

    public ApiInterceptor[] getInterceptors() {
        return interceptors;
    }

    public void setInterceptors(ApiInterceptor[] interceptors) {
        this.interceptors = interceptors;
    }
    
    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }
    
    public String getDefaultVersion() {
        return defaultVersion;
    }

    public void setDefaultVersion(String defaultVersion) {
        this.defaultVersion = defaultVersion;
    }

    public ParamParser getParamParser() {
        return paramParser;
    }

    public void setParamParser(ParamParser paramParser) {
        this.paramParser = paramParser;
    }

    public String getPriKeyPath() {
        return priKeyPath;
    }

    public void setPriKeyPath(String priKeyPath) {
        this.priKeyPath = priKeyPath;
    }
    
    public SessionManager getSessionManager() {
        return sessionManager;
    }

    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }
    
    public String getDocClassPath() {
        return docClassPath;
    }

    public void setDocClassPath(String docClassPath) {
        this.docClassPath = docClassPath;
    }

    public MonitorStore getMonitorStore() {
        return monitorStore;
    }

    public void setMonitorStore(MonitorStore monitorStore) {
        this.monitorStore = monitorStore;
    }
    

    public ApiInterceptor getMonitorInerceptor() {
        return monitorInerceptor;
    }

    public void setMonitorInerceptor(ApiInterceptor monitorInerceptor) {
        this.monitorInerceptor = monitorInerceptor;
    }
    
    public String getMonitorClassPath() {
        return monitorClassPath;
    }

    public void setMonitorClassPath(String monitorClassPath) {
        this.monitorClassPath = monitorClassPath;
    }
    
    public int getMonitorErrorQueueSize() {
        return monitorErrorQueueSize;
    }

    public void setMonitorErrorQueueSize(int monitorErrorQueueSize) {
        this.monitorErrorQueueSize = monitorErrorQueueSize;
    }

    public int getMonitorExecutorSize() {
        return monitorExecutorSize;
    }

    public void setMonitorExecutorSize(int monitorExecutorSize) {
        this.monitorExecutorSize = monitorExecutorSize;
    }
    

    public String getMonitorPassword() {
        return monitorPassword;
    }

    public void setMonitorPassword(String monitorPassword) {
        this.monitorPassword = monitorPassword;
    }

    // =====================================
    public void setApiName(String apiName) {
        ParamNames.API_NAME = apiName;
    }

    public void setVersionName(String versionName) {
        ParamNames.VERSION_NAME = versionName;
    }

    public void setAppKeyName(String appKeyName) {
        ParamNames.APP_KEY_NAME = appKeyName;
    }

    public void setDataName(String dataName) {
        ParamNames.DATA_NAME = dataName;
    }

    public void setTimestampName(String timestampName) {
        ParamNames.TIMESTAMP_NAME = timestampName;
    }

    public void setSignName(String signName) {
        ParamNames.SIGN_NAME = signName;
    }

}
