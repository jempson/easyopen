package com.gitee.easyopen;

/**
 * 校验接口
 * 
 * @author tanghc
 *
 */
public interface Validator extends HasConfig {
    void validate(ApiParam param);
    
    /**
     * 验证业务参数
     * 
     * @param obj
     */
    void validateBusiParam(Object obj);
}
