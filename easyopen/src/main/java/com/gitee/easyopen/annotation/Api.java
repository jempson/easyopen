package com.gitee.easyopen.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 作用在service类的方法上，service类被@ApiService标记
 * @author tanghc
 *
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface Api {

    /**
     * 接口名，建议命名规则:名字.动词，如：goods.get，taobao.open.account.list
     * 
     * @return
     */
    String name();

    /**
     * 接口版本号，默认"",建议命名规则：x.y，如1.0，1.1
     * 
     * @return
     */
    String version() default "";

    /**
     * 忽略验证签名,默认false.为true接口不执行验签操作,但其它验证会执行.
     * @return
     */
    boolean ignoreSign() default false;
    
    /**
     * 忽略所有验证,默认false.为true接口都不执行任何验证操作.
     * @return
     */
    boolean ignoreValidate() default false;
    
    /**
     * 是否对返回结果进行包装,如果设置成false,则直接返回业务方法结果.
     */
    boolean wrapResult() default true;
}
