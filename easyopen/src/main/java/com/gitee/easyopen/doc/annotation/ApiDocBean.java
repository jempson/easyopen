package com.gitee.easyopen.doc.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface ApiDocBean {

    /**
     * 描述
     * 
     * @return
     */
    String description() default "";

    /**
     * 字段信息
     * 
     * @return
     */
    ApiDocField[] fields() default {};
}
