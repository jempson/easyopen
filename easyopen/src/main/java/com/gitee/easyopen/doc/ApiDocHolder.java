package com.gitee.easyopen.doc;

public class ApiDocHolder {
    private static ApiDocBuilder apiDocBuilder;

    public static ApiDocBuilder getApiDocBuilder() {
        return apiDocBuilder;
    }

    public static void setApiDocBuilder(ApiDocBuilder apiDocBuilder) {
        ApiDocHolder.apiDocBuilder = apiDocBuilder;
    }

}
