package com.gitee.easyopen.sdk;

import java.net.URLEncoder;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gitee.easyopen.sdk.modal.Goods;
import com.gitee.easyopen.sdk.param.GoodsParam;
import com.gitee.easyopen.sdk.req.GoodsReq;
import com.gitee.easyopen.sdk.resp.GoodsResp;
import com.gitee.easyopen.sdk.util.PostUtil;

public class SdkTest extends BaseTest {

    @Test
    public void testGet() throws Exception {
        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("goods.get", param);

        GoodsResp result = client.request(req);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            System.out.println("errorMsg:" + result.getMsg());
        }
        System.out.println("--------------------");
    }
    
    @Test
    public void testGet2() throws Exception {

        GoodsParam param = new GoodsParam();
        //param.setGoods_name("iphone6");
        GoodsReq req = new GoodsReq("goods.get", param);

        GoodsResp result = client.request(req);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            System.out.println("errorMsg:" + result.getMsg());
        }
        System.out.println("--------------------");
    }
    
    // 先浏览器访问http://localhost:8080/go_oauth2 获取accessToken
    @Test
    public void testGetToken() throws Exception {
        String accessToken = "25f4a244e92e8c22e0964193733b4e77";

        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        
        GoodsReq req = new GoodsReq("user.goods.get", param);
        req.setAccess_token(accessToken);
        
        GoodsResp result = client.request(req);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            throw new RuntimeException(result.getMsg());
        }
        System.out.println("--------------------");

    }
    
    // 先浏览器访问http://localhost:8080/jwtLogin 获取jwt
    @Test
    public void testGetWithJwt() throws Exception {
        OpenClient client2 = new OpenClient(url, appKey, secret);
        String jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIyIiwiZXhwIjoxNTI2OTU5MzYxLCJpYXQiOjE1MjY5NTIxNjEsInVzZXJuYW1lIjoiamltIn0.KRRgwEfWQsgjerHHjYBF0S6V0QSD-l-ELj9plcLd-Fs";

        GoodsParam param = new GoodsParam();
        param.setGoods_name("iphone6");
        
        GoodsReq req = new GoodsReq("userjwt.goods.get", param);
        
        GoodsResp result = client2.requestWithJwt(req, jwt);
        
        System.out.println("--------------------");
        if (result.isSuccess()) {
            Goods goods = result.getBody();
            System.out.println(goods);
        } else {
            throw new RuntimeException(result.getMsg());
        }
        System.out.println("--------------------");

    }
    
    @Test
    public void testAdd() throws Exception {
        JSONObject param = new JSONObject();

        GoodsParam goods = new GoodsParam();
        goods.setGoods_name("333333333333333333333333333333333333");

        param.put("name", "goods.add");
        param.put("data", URLEncoder.encode(JSON.toJSONString(goods), "UTF-8"));
        param.put("version", "");

        System.out.println("--------------------");
        System.out.println("请求内容:" + JSON.toJSONString(param));
        
        String resp = PostUtil.postString(url, param);

        System.out.println(resp);
        System.out.println("--------------------");
    }

}
