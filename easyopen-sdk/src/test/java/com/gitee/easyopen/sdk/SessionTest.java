package com.gitee.easyopen.sdk;

import org.junit.Test;

import com.gitee.easyopen.sdk.req.SessionReq;
import com.gitee.easyopen.sdk.resp.SessionResp;
import com.gitee.easyopen.sdk.util.PostUtil;

public class SessionTest extends BaseTest {

    // 演示session功能
    @Test
    public void testSession() throws Exception {
        // 设置session值
        SessionReq setReq = new SessionReq("session.set");
        SessionResp resp = client.request(setReq);
        System.out.println(resp.getBody());
        
        // 获取session值
        SessionReq getReq = new SessionReq("session.get");
        SessionResp respGet = client.request(getReq);
        System.out.println("session中的值:" + respGet.getBody());
    }
    
    @Test
    public void testManagedSession() throws Exception {
        String sessionId = PostUtil.postString("http://localhost:8080/managedSessionLogin", null);
        System.out.println("sessionId:"+sessionId);
        // SessionApi.java
        SessionReq req = new SessionReq("manager.session.get");
        req.setAccess_token(sessionId);
        
        SessionResp respGet = client.request(req);
        
        System.out.println("session中的值:" + respGet.getBody());
    }

}
