package com.gitee.easyopen.sdk;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import com.gitee.easyopen.sdk.resp.ErrorResp;
import com.gitee.easyopen.sdk.util.AESUtil;
import com.gitee.easyopen.sdk.util.ClassUtil;
import com.gitee.easyopen.sdk.util.JsonUtil;
import com.gitee.easyopen.sdk.util.MD5Util;
import com.gitee.easyopen.sdk.util.PostUtil;
import com.gitee.easyopen.sdk.util.RSAUtil;
import com.gitee.easyopen.sdk.util.SignUtil;

/**
 * 请求客户端，创建一个即可。
 * 
 * @author hc.tang
 *
 */
public class OpenClient {

    private static final String UTF8 = "UTF-8";
    private static final String ZH_CN = "zh-CN";

    private static final String ACCEPT_LANGUAGE = "Accept-Language";
    private static final String AUTHORIZATION = "Authorization";
    private static final String PREFIX_BEARER = "Bearer ";

    private static String publicKey; // 公钥数据
    private static String randomKey; // 随机码

    private String url;
    private String appKey;
    private String secret;
    private String lang = ZH_CN;

    private String appKeyName = "app_key";
    private String signName = "sign";
    private String dataName = "data";

    public OpenClient(String url, String appKey, String secret, String lang) {
        this(url, appKey, secret);
        this.lang = lang;
    }

    public OpenClient(String url, String appKey, String secret) {
        this.url = url;
        this.appKey = appKey;
        this.secret = secret;

        init(url);
    }

    private void init(String url) {
        if (this.getRequestMode() == RequestMode.ENCRYPT) {
            initRandomKey();
            initPublicKey();
            try {
                // 传递随机数
                String randomKeyEncrypted = createRandomKeyByPublicKey();
                url = url + "/handshake";
                String json = PostUtil.postText(url, randomKeyEncrypted);
                checkResp(json);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("初始化失败", e);
            }
        }
    }

    private static void initRandomKey() {
        randomKey = MD5Util.encrypt16(UUID.randomUUID().toString());
    }

    private static void initPublicKey() {
        InputStream input = OpenClient.class.getClassLoader().getResourceAsStream("pub.key");
        if (input != null) {
            InputStreamReader in = null;
            StringWriter writer = null;
            try {
                in = new InputStreamReader(input, "UTF-8");
                writer = new StringWriter();
                char[] buffer = new char[4096];
                int EOF = -1;
                int n;
                while (EOF != (n = in.read(buffer))) {
                    writer.write(buffer, 0, n);
                }
                publicKey = writer.toString();
            } catch (Exception e) {
            } finally {
                try {
                    in.close();
                    writer.close();
                } catch (IOException e) {
                }
            }
        }
        if (publicKey == null || "".equals(publicKey)) {
            throw new RuntimeException("加载公钥失败");
        }
    }

    private static String createRandomKeyByPublicKey() throws Exception {
        return RSAUtil.encryptByPublicKey(randomKey, RSAUtil.getPublicKey(publicKey));
    }

    private static void checkResp(String resp) throws Exception {
        ApiResult result = JsonUtil.parseOject(resp, ApiResult.class);
        if (!"0".equals(result.getCode())) {
            throw new RuntimeException(result.getMsg());
        }

        String data = String.valueOf(result.getData());
        String desStr = RSAUtil.decryptByPublicKey(data, RSAUtil.getPublicKey(publicKey));

        String content = AESUtil.decryptFromHex(desStr, randomKey);
        // 一致
        boolean same = "0".equals(content);

        if (!same) {
            throw new RuntimeException("数据传输有误");
        }
    }

    public <T> T request(BaseReq<T> request) {
        return request(request, null);
    }

    public <T> T requestWithJwt(BaseReq<T> request, String jwt) {
        return request(request, jwt);
    }

    private <T> T request(BaseReq<T> request, String jwt) {
        Map<String, Object> params = ClassUtil.convertObj2Map(request);
        String data = JsonUtil.toJson(params.get(dataName));
        if (data == null || "".equals(data)) {
            data = "{}";
        }
        try {
            data = URLEncoder.encode(data, UTF8);
            params.put(dataName, data);
        } catch (UnsupportedEncodingException e) {
        }
        params.put(appKeyName, this.appKey);
        String body = this.post(url, params, jwt);
        T t = (T) JsonUtil.parseOject(body, request.buildRespClass());
        return t;
    }

    /**
     * 发送请求
     * 
     * @param url
     *            请求连接
     * @param params
     *            请求参数
     * @return 服务端返回的内容
     */
    private String post(String url, Map<String, Object> params, String jwt) {
        String jsonParam = null;
        RequestMode requestMode = this.getRequestMode();
        Map<String, String> header = buildHeader();

        if (jwt != null) {
            header.put(AUTHORIZATION, PREFIX_BEARER + jwt);
        }

        if (requestMode == RequestMode.SIGNATURE) {
            Map<String, Object> param = buildSignTypeParam(params);
            jsonParam = JsonUtil.toJson(param);
        } else if (requestMode == RequestMode.ENCRYPT) {
            try {
                jsonParam = this.buildRsaContent(params);
                this.encryptHeader(header);
                url = url + "/ssl";
            } catch (Exception e) {
                throw new RuntimeException("构建参数失败", e);
            }
        } else {
            throw new RuntimeException("找不到请求模式:" + requestMode.name());
        }

        return this.doPost(url, jsonParam, header);
    }

    // 加密header
    private void encryptHeader(Map<String, String> header) throws Exception {
        Set<Entry<String, String>> entrySet = header.entrySet();
        for (Entry<String, String> entry : entrySet) {
            String key = entry.getKey();
            String value = entry.getValue();

            value = doAes(value, randomKey);

            header.put(key, value);
        }
    }

    private Map<String, String> buildHeader() {
        Map<String, String> header = new HashMap<String, String>();
        header.put(ACCEPT_LANGUAGE, lang);
        return header;
    }

    private String buildRsaContent(Map<String, Object> params) throws Exception {
        return doAes(JsonUtil.toJson(params), randomKey);
    }

    protected String doAes(String content, String key) throws Exception {
        return AESUtil.encryptToHex(content, key);
    }

    private Map<String, Object> buildSignTypeParam(Map<String, Object> params) {
        String sign = null;
        try {
            sign = SignUtil.buildSign(params, this.secret);
        } catch (IOException e) {
            throw new SdkException("签名构建失败");
        }

        params.put(signName, sign);

        return params;
    }

    private String doPost(String url, String params, Map<String, String> header) {
        try {
            String body = PostUtil.postJson(url, params, header);
            // 对结果进行解密
            if (this.getRequestMode() == RequestMode.ENCRYPT) {
                body = AESUtil.decryptFromHex(body, randomKey);
            }
            return body;
        } catch (Exception e) {
            ErrorResp result = new ErrorResp();
            result.setCode("-1");
            result.setMsg(e.getMessage());
            return JsonUtil.toJson(result);
        }
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public RequestMode getRequestMode() {
        return RequestMode.SIGNATURE;
    }

}
