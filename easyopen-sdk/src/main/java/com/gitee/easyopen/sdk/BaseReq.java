package com.gitee.easyopen.sdk;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.gitee.easyopen.sdk.util.ClassUtil;

/**
 * @author tanghc
 *
 * @param <T> 返回类
 */
public abstract class BaseReq<T> {

    private static final String FORMAT_JSON = "json";
    private static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private String name;
    private String version = SdkConfig.defaultVersion;
    private Object data;
    private String timestamp = new SimpleDateFormat(TIME_FORMAT).format(new Date());
    private String access_token;
    private String sign_method = "md5";
    
    private Class<T> respClass;

    @SuppressWarnings("unchecked")
    public BaseReq() {
        respClass = (Class<T>)ClassUtil.getSuperClassGenricType(getClass(), 0);
    }

    public BaseReq(String name, Object data) {
        this();
        this.name = name;
        this.data = data;
    }

    public BaseReq(String name, String version, Object data) {
        this();
        this.name = name;
        this.version = version;
        this.data = data;
    }

    public Class<T> buildRespClass() {
        return respClass;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getFormat() {
        return FORMAT_JSON;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getSign_method() {
        return sign_method;
    }

    public void setSign_method(String sign_method) {
        this.sign_method = sign_method;
    }

}
