package com.gitee.easyopen.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyopenServer2Application {

	public static void main(String[] args) {
		SpringApplication.run(EasyopenServer2Application.class, args);
	}
}
