package com.gitee.easyopen.server;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gitee.easyopen.ApiConfig;
import com.gitee.easyopen.support.ApiController;

import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/api")
public class IndexController extends ApiController {
    
    // http://localhost:8080/api/mono
    @RequestMapping("mono")
    @ResponseBody
    public Mono<Object> mono(HttpServletRequest request, HttpServletResponse response) {
        return Mono.just(this.invoke(request, response));
    }
    
    @Override
    protected void initApiConfig(ApiConfig apiConfig) {
        apiConfig.setShowDoc(true); // 显示文档页面
        // 配置国际化消息
        apiConfig.getIsvModules().add("i18n/isv/goods_error");

        // 配置秘钥键值对
        Map<String, String> appSecretStore = new HashMap<String, String>();
        appSecretStore.put("test", "123456");

        apiConfig.addAppSecret(appSecretStore);
    }

}

