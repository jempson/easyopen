package com.gitee.easyopen.server.api;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.gitee.easyopen.annotation.Api;
import com.gitee.easyopen.annotation.ApiService;
import com.gitee.easyopen.doc.DataType;
import com.gitee.easyopen.doc.annotation.ApiDoc;
import com.gitee.easyopen.doc.annotation.ApiDocBean;
import com.gitee.easyopen.doc.annotation.ApiDocField;
import com.gitee.easyopen.doc.annotation.ApiDocMethod;
import com.gitee.easyopen.server.model.Goods;
import com.gitee.easyopen.server.model.GoodsParam;
import com.gitee.easyopen.server.model.PageInfo;

/**
 * 文档定义demo
 * @author tanghc 
 */
@ApiService
@ApiDoc("文档demo，参考DocDemoApi.java")
public class DocDemoApi {
    
    static Goods goods = new Goods();
    static {
        goods.setId(1L);
        goods.setGoods_name("苹果iPhoneX");
        goods.setPrice(new BigDecimal(8000));
    }

    // 参数方式1，参数类GoodsParam@ApiDocField
    @Api(name = "doc.param.1")
    @ApiDocMethod(description = "参数方式1,默认", remark = "参数类GoodsParam@ApiDocField")
    public Goods demo1(GoodsParam param) {
        return goods;
    }

    // 参数方式2，指定参数类Demo2Param，Demo2Param继承GoodsParam内容
    @Api(name = "doc.param.2")
    @ApiDocMethod(description = "参数方式2,继承", paramClass = Demo2Param.class, remark = "指定参数类Demo2Param，Demo2Param继承GoodsParam内容")
    public Goods demo2(GoodsParam param) {
        return goods;
    }

    // 参数方式3，参数里面有参数，Demo3Param内聚合GoodsParam
    @Api(name = "doc.param.3")
    @ApiDocMethod(description = "参数方式3,聚合", remark = "参数里面有参数，Demo3Param内聚合GoodsParam")
    public Goods demo3(Demo3Param param) {
        return goods;
    }

    // 参数方式4，自定义属性
    @Api(name = "doc.param.4")
    @ApiDocMethod(description = "参数方式4,自定义属性", params = { 
            @ApiDocField(name = "id", dataType = DataType.INT),
            @ApiDocField(name = "goodsName", dataType = DataType.STRING, description = "商品名称"), 
    }, remark = "自定义属性")
    public Goods param4(GoodsParam param) {
        return goods;
    }
    
    // 参数方式5，外部类指定参数，可复用
    @Api(name = "doc.param.5")
    @ApiDocMethod(description = "参数方式5，外部类指定参数，可复用", paramClass = DemoTopParam.class, remark = "参数方式5，外部类指定参数，可复用")
    public Goods param5(JSONObject param) {
        return goods;
    }

    // 返回结果1，默认
    @Api(name = "doc.result.1")
    @ApiDocMethod(description = "返回结果1,默认")
    public Goods result1(GoodsParam param) {
        return goods;
    }

    // 返回结果2，使用指定的返回结果类
    @Api(name = "doc.result.2")
    @ApiDocMethod(description = "返回结果2,指定返回结果类", resultClass = Result1.class, remark = "使用指定的返回结果类")
    public Goods result2(GoodsParam param) {
        return goods;
    }

    // 返回结果3，自定义返回字段
    @Api(name = "doc.result.3")
    @ApiDocMethod(description = "返回结果3,自定义字段", results = { 
            @ApiDocField(name = "id", description = "id"),
            @ApiDocField(name = "remark", description = "备注") 
    }, remark = "自定义返回字段")
    public Goods result3(GoodsParam param) {
        return goods;
    }
    
    // 返回结果4
    @Api(name = "doc.result.4")
    @ApiDocMethod(description = "返回结果4,返回List", elementClass=Goods.class, remark = "自定义返回字段")
    public List<Goods> result4(GoodsParam param) {
        return Arrays.asList(goods);
    }
    
    // 返回结果5
    @Api(name = "doc.result.5")
    @ApiDocMethod(description="返回结果5,外部指定"
            ,results={@ApiDocField(name="pageIndex",description="第几页",dataType=DataType.INT,example="1"),
                    @ApiDocField(name="pageSize",description="每页几条数据",dataType=DataType.INT,example="10"),
                    @ApiDocField(name="total",description="每页几条数据",dataType=DataType.LONG,example="100"),
                    @ApiDocField(name="rows",description="数据",dataType=DataType.ARRAY,elementClass=Goods.class),}
            )
    // 假设PageInfo是jar中的类，没法修改。但是要对其进行文档生成
    public PageInfo<Goods> pageinfo(GoodsParam param) {
        
        List<Goods> list = Arrays.asList(goods, goods);
        
        PageInfo<Goods> pageInfo = new PageInfo<>();
        pageInfo.setPageIndex(1);
        pageInfo.setPageSize(10);
        pageInfo.setTotal(100);
        pageInfo.setRows(list);
        
        return pageInfo;
    }
    
    // 返回结果6，自定义返回字段,包含自定义类
    @Api(name = "doc.result.6")
    @ApiDocMethod(description = "返回结果6,自定义类", results = { 
            @ApiDocField(name = "id", description = "id"),
            @ApiDocField(name = "remark", description = "备注"),
            @ApiDocField(name = "result1", description = "result1",beanClass=Result1.class), // 自定义类
            
    }, remark = "自定义返回字段,包含自定义类")
    public Goods result6(GoodsParam param) {
        return goods;
    }
    
    @Api(name = "doc.result.7")
    @ApiDocMethod(description="返回结果7,模板复用",resultClass = GoodsVo.class)
    public Goods result7(GoodsParam param) {
        return goods;
    }
    
    
    
    
    //////////////////////////////
    
    // 参数模板类，可复用
    @ApiDocBean(fields = {
        @ApiDocField(name = "id", description = "id"),
        @ApiDocField(name="pageIndex",description="第几页",dataType=DataType.INT,example="1"),
        @ApiDocField(name="pageSize",description="每页几条数据",dataType=DataType.INT,example="10"),
    })
    public static class DemoTopParam {
    }

    public static class Demo2Param extends GoodsParam { // 继承
        @ApiDocField(description = "id列表", elementClass = Integer.class)
        private List<Integer> idList;

        public List<Integer> getIdList() {
            return idList;
        }

        public void setIdList(List<Integer> idList) {
            this.idList = idList;
        }
    }

    public static class Demo3Param {
        @ApiDocField(name = "id", description = "id", required = true, example = "1")
        private int id;
        @ApiDocField(name = "param", description = "GoodsParam", required = true)
        private GoodsParam param; // 另外一个参数

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public GoodsParam getParam() {
            return param;
        }

        public void setParam(GoodsParam param) {
            this.param = param;
        }
    }

    public static class Result1 extends Goods {
        @ApiDocField(description = "备注")
        private String remark;

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }
    }
    
    // 参数模板
    @ApiDocBean(fields = {
        @ApiDocField(name="pageIndex",description="第几页",dataType=DataType.INT,example="1"),
        @ApiDocField(name="pageSize",description="每页几条数据",dataType=DataType.INT,example="10"),
        @ApiDocField(name="total",description="每页几条数据",dataType=DataType.LONG,example="100"),
        @ApiDocField(name="rows",description="商品列表",dataType=DataType.ARRAY,elementClass=Goods.class),
    })
    public static class GoodsVo extends PageInfo<Goods> {
    }


}
