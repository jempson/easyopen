package com.gitee.easyopen.server.model;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import com.gitee.easyopen.AppSecretManager;

/**
 * 使用方式:
 * 
 * <pre>
@Autowired
private AppSecretManager appSecretManager;
    
@Override
protected void initApiConfig(ApiConfig apiConfig) {
    ...
    apiConfig.setAppSecretManager(appSecretManager);
    ...
}   

 * </pre>
 * 
 * @author tanghc
 *
 */
//@Component
public class RedisAppSecretManager implements AppSecretManager {

    public static String APP_KEY_PREFIX = "easyopen_app_key:";
    
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    
    
    @Override
    public void addAppSecret(Map<String, String> appSecretStore) {
        stringRedisTemplate.opsForHash().putAll(APP_KEY_PREFIX, appSecretStore);
    }

    @Override
    public String getSecret(String appKey) {
        return (String)stringRedisTemplate.opsForHash().get(APP_KEY_PREFIX, appKey);
    }

    @Override
    public boolean isValidAppKey(String appKey) {
        if (appKey == null) {
            return false;
        }
        return getSecret(appKey) != null;
    }

}
