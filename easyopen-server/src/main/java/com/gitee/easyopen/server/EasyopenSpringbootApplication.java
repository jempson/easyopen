package com.gitee.easyopen.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
public class EasyopenSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasyopenSpringbootApplication.class, args);
	}
}
